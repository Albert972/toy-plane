#include "stdafx.h"
#include "string.h"
#include "stdio.h"
#include <iostream>
#include "conio.h"
#include <windows.h>
#include "stdlib.h"
#include <ctime>
using namespace std;

class Power
{
public:
	int petrol; // This variable shows petrol amount...
	Power(int);
	Power(const Power& obj);
	void set_p(int);
	virtual void view()
	{
		cout << "Power:" << petrol << endl;
	}
	int get_p();
	~Power(){};
};
Power::Power(int p)
{
	petrol = p;
}
int Power::get_p(){ return petrol; };
void Power::set_p(int p){ this->petrol = p; };

class Height
{
public:
	int height;
	Height(int);
	void set_h(int);
	int get_h();
	~Height(){};
};
Height::Height(int h)
{
	height = h;
}
int Height::get_h(){ return height; };
void Height::set_h(int h){ this->height = h; };

class Direction
{
public:
	int up, down, left, right;
	Direction(int, int, int, int);
	void set_u(int);
	void set_d(int);
	void set_l(int);
	void set_r(int);
	int get_u();
	int get_d();
	int get_l();
	int get_r();
	~Direction(){};
};
Direction::Direction(int u, int d, int l, int r)
{
	up = u;
	down = d;
	left = l;
	right = r;
}
int Direction::get_u(){ return up; };
int Direction::get_d(){ return down; };
int Direction::get_l(){ return left; };
int Direction::get_r(){ return right; };
void Direction::set_u(int u){ this->up = u; };
void Direction::set_d(int d){ this->down = d; };
void Direction::set_l(int l){ this->left = l; };
void Direction::set_r(int r){ this->right = r; };
time_t t = time(0);

int _tmain(int argc, _TCHAR* argv[])
{

	int up, down, left, right;
	//char ch;
	//int code;
	int p, x;
	system("cls");
	cout << "Are you ready?!" << endl;
	system("pause");
	system("cls");
	cout << "GO!" << endl;
	system("pause");
	/*while (true)
	{
	ch = _getche();
	code = static_cast<int>(ch);
	if (ch == 27) //
	exit(0);//
	}*/
	srand(time(0)); // 
	int h,d;
	h = 10;
	d = 0;
	p = 100;

	while ((p > 0) && (h>0)&&(h<160)&&(d<160))
	{
		system("cls");
		x = 1 + rand() % 2;
		time_t t1 = time(0);
		time_t t11 = t1 - t;
		
		time_t rawtime;
		struct tm * timeinfo;

		time(&rawtime);
		timeinfo = localtime(&rawtime);
		printf("Current local time and date: %s", asctime(timeinfo));
		if (p < 0)
		{
			cout << "The battery is low :(" << endl;
			cout << "Please, feed me" << endl;
		}
		else
		cout << "Balance battery  " << p << "  %" << endl;
		if (x != 1)
		{
			cout << "Up" << endl;
			h = h + t11/2;
			p = p - (t11 *h)/33;
			
		}
		else
		{
			cout << "Down" << endl;
			h = h - t11/2;
			p = p - (t11 * h)/66;
			
		}
		d = d + t11;
		if (d >= 160)
			cout << "Wall crept unexpectedly 0_o" << endl;
		else
		cout << "Distance " << d << endl;
		if ((h > 0) && (h < 160))
		{
			cout << "Height  " << h << endl;
		}
		else
		{
			if (h >= 160)
			{
				cout << "Whoops! It was the ceiling!" << endl;
			}
			if (h <= 0)
			{
				cout << "Hello, floor!" << endl;
			}
		}
		cout << "Flight time "<< t1-t <<" seconds"<< endl;
		Sleep(500);
	}
	cout << "\n";
	cout << "End" << endl;
	system("pause");
	return 0;
}